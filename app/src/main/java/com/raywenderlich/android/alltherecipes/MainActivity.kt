package com.raywenderlich.android.alltherecipes

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ListView


class MainActivity : AppCompatActivity() {

  private lateinit var listView: ListView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    listView = findViewById<ListView>(R.id.recipe_list_view)

    val recipeList = Recipe.getRecipesFromFile("recipes.json", this)

    val adapter = RecipeAdapter(this, recipeList)
    listView.adapter = adapter

    val context = this
    listView.setOnItemClickListener { _, _, position, _ ->
      val selectedRecipe = recipeList[position]

      val detailIntent = RecipeDetailActivity.newIntent(context, selectedRecipe)

      startActivity(detailIntent)
    }
  }
}
